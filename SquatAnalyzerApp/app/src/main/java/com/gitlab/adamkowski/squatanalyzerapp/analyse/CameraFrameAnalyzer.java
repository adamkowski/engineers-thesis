package com.gitlab.adamkowski.squatanalyzerapp.analyse;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.util.Log;
import android.util.Size;

import androidx.annotation.NonNull;
import androidx.camera.core.ImageAnalysis;
import androidx.camera.core.ImageProxy;

import com.gitlab.adamkowski.squatanalyzerapp.api.Training;
import com.gitlab.adamkowski.squatanalyzerapp.impl.SquatsTraining;
import com.gitlab.adamkowski.squatanalyzerapp.posenet.BodyPart;
import com.gitlab.adamkowski.squatanalyzerapp.posenet.KeyPoint;
import com.gitlab.adamkowski.squatanalyzerapp.posenet.Person;
import com.gitlab.adamkowski.squatanalyzerapp.posenet.PoseEstimator;
import com.gitlab.adamkowski.squatanalyzerapp.posenet.TensorFlowPoseNetModel;
import com.gitlab.adamkowski.squatanalyzerapp.ui.EvaluationMessenger;
import com.gitlab.adamkowski.squatanalyzerapp.ui.PosePreviewProvider;
import com.gitlab.adamkowski.squatanalyzerapp.ui.TrainingPhaseMessenger;
import com.gitlab.adamkowski.squatanalyzerapp.utils.ImageConverter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static com.gitlab.adamkowski.squatanalyzerapp.ui.TrainingPhaseMessenger.STATE_MESSAGE;

public class CameraFrameAnalyzer implements ImageAnalysis.Analyzer {
    private static final int FRAMES_AMOUNT = 3;
    private static final float POSE_DETECTION_THRESHOLD = 0.25f;
    private static final long REQUIRED_IDLE_MILLISECONDS = 3000;

    private final PoseEstimator poseEstimator;
    private final List<List<KeyPoint>> lastFramesKeyPoints = new ArrayList<>(FRAMES_AMOUNT);

    private final Training training = new SquatsTraining();

    private long idleTimeStart = System.currentTimeMillis();
    private Person idleStatePerson = null;
    private boolean idle = false;

    private final TrainingPhaseMessenger trainingPhaseMessenger;
    private final EvaluationMessenger evaluationMessenger;
    private final PosePreviewProvider posePreviewProvider;

    public CameraFrameAnalyzer(Context context) {
        this.poseEstimator = new PoseEstimator(context);
        this.trainingPhaseMessenger = new TrainingPhaseMessenger(context);
        this.evaluationMessenger = new EvaluationMessenger(context, training);
        this.posePreviewProvider = new PosePreviewProvider(context, training);
    }

    @Override
    public void analyze(@NonNull ImageProxy image) {
        Bitmap bitmap = prepareBitmap(image, image.getHeight(), image.getWidth());
        image.close();

        Person estimatedPose = poseEstimator.estimateSinglePose(bitmap);
        Person person = reduceNoise(estimatedPose);

        if (isPersonDetected(person)) {
            posePreviewProvider.drawPose(person);
            analyzeTraining(person);
        } else {
            posePreviewProvider.clearPreview();
        }

        evaluationMessenger.showDelayInfo();
    }

    private void analyzeTraining(Person person) {
        final long currentTimeMs = System.currentTimeMillis();
        Training.TrainingPhase trainingPhase = training.getPhase();
        if (isTrainingInProgress(trainingPhase)) {
            if (idleStatePerson != null && !isPersonInMotion(person)) {
                checkIdleStateChange(currentTimeMs);
                if (trainingPhase == Training.TrainingPhase.READY) {
                    if (idle) {
                        trainingPhaseMessenger.informUser(STATE_MESSAGE.IN_PROGRESS);
                        training.start(person);
                        idle = false;
                    }
                } else if (trainingPhase == Training.TrainingPhase.STARTED) {
                    if (!idle) {
                        int trainingResult = training.evaluate(person);
                        evaluationMessenger.informUser(trainingResult);
                    } else {
                        trainingPhaseMessenger.informUser(STATE_MESSAGE.FINISH);
                        training.finish();
                    }
                }
            } else {
                initIdleState(person, currentTimeMs);
            }
        }
    }

    private Bitmap prepareBitmap(ImageProxy image, int height, int width) {
        final int size = TensorFlowPoseNetModel.SIZE;
        int[] rgbBytes = readRgbBytes(image);
        return Stream.of(Bitmap.createBitmap(rgbBytes, width, height, Bitmap.Config.ARGB_8888))
                .map(ImageConverter::cropBitmapToSquare)
                .map(bitmap -> Bitmap.createScaledBitmap(bitmap, size, size, true))
                .map(ImageConverter::rotateBitmapToPortraitMode)
                .findFirst()
                .orElse(Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888));
    }

    private int[] readRgbBytes(ImageProxy image) {
        ImageProxy.PlaneProxy[] imagePlanes = image.getPlanes();
        Size imageSize = new Size(image.getWidth(), image.getHeight());
        return ImageConverter.convertFrameYUVToRGB(imagePlanes, imageSize);
    }

    private Person reduceNoise(Person person) {
        if (lastFramesKeyPoints.size() == FRAMES_AMOUNT) {
            lastFramesKeyPoints.remove(0);
        }
        lastFramesKeyPoints.add(person.keyPoints);

        return new Person(averageKeyPoints(), person.score);
    }

    private List<KeyPoint> averageKeyPoints() {
        final int keyPointsAmount = lastFramesKeyPoints.get(0).size();
        final int framesAmount = lastFramesKeyPoints.size();
        return IntStream.range(0, keyPointsAmount)
                .mapToObj(keyPoint -> averagePointCoordinates(framesAmount, keyPoint))
                .collect(Collectors.toList());
    }

    private KeyPoint averagePointCoordinates(int framesAmount, int keyPoint) {
        int sumX = 0;
        int sumY = 0;
        float sumScore = 0;
        for (int frameNumber = 0; frameNumber < framesAmount; frameNumber++) {
            KeyPoint point = lastFramesKeyPoints.get(frameNumber).get(keyPoint);
            sumX += point.position.x;
            sumY += point.position.y;
            sumScore += point.score;
        }

        BodyPart bodyPart = lastFramesKeyPoints.get(0).get(keyPoint).bodyPart;
        Point avgPoint = new Point(sumX / framesAmount, sumY / framesAmount);
        float avgScore = sumScore / framesAmount;
        return new KeyPoint(bodyPart, avgPoint, avgScore);
    }

    private boolean isTrainingInProgress(Training.TrainingPhase trainingPhase) {
        return trainingPhase != Training.TrainingPhase.FINISHED;
    }

    private boolean isPersonDetected(Person person) {
        return person.score >= POSE_DETECTION_THRESHOLD;
    }

    private boolean isPersonInMotion(Person person) {
        final int allowedNoise = 15;
        for (int pointIndex = 0; pointIndex < person.keyPoints.size(); pointIndex++) {
            Point current = person.keyPoints.get(pointIndex).position;
            Point idle = this.idleStatePerson.keyPoints.get(pointIndex).position;
            if ((current.x > idle.x + allowedNoise || current.x < idle.x - allowedNoise) ||
                    (current.y > idle.y + allowedNoise || current.y < idle.y - allowedNoise)) {
                return true;
            }
        }
        return false;
    }

    private void initIdleState(Person person, long currentTimeMs) {
        idleStatePerson = person;
        idleTimeStart = currentTimeMs;
    }

    private void checkIdleStateChange(long currentTimeMs) {
        Log.d("training", "idle time: " + (currentTimeMs - idleTimeStart));
        if (currentTimeMs - idleTimeStart >= REQUIRED_IDLE_MILLISECONDS) {
            idleTimeStart = currentTimeMs;
            idle = !idle;
        }
    }
}
