package com.gitlab.adamkowski.squatanalyzerapp.api;

import com.gitlab.adamkowski.squatanalyzerapp.analyse.CameraFrameAnalyzer;
import com.gitlab.adamkowski.squatanalyzerapp.posenet.BodyPart;
import com.gitlab.adamkowski.squatanalyzerapp.posenet.KeyPoint;
import com.gitlab.adamkowski.squatanalyzerapp.posenet.Person;

import java.util.List;

/**
 * Represents how an exercise is evaluated. Implementations of this interface are used by the
 * {@link CameraFrameAnalyzer} class.
 */
public interface Training {

    /**
     * Code returned by the {@link #evaluate(Person)} method if the exercise is being evaluated.
     */
    int EVALUATION_IN_PROGRESS = 0;

    /**
     * Phases in which training can be detected.
     */
    enum TrainingPhase {
        /**
         * The person is moving and getting ready to exercise.
         */
        READY,

        /**
         * The person performs the exercises.
         */
        STARTED,

        /**
         * The evaluation is finished.
         */
        FINISHED
    }

    /**
     * Returns the current training phase.
     */
    TrainingPhase getPhase();

    /**
     * Return the list of {@link BodyPart} used to evaluate the exercises. The order of elements
     * in the list must allow the points to be connected with a line, for example, shoulder, hip,
     * knee.
     */
    List<BodyPart> getEvaluatedBodyParts();

    /**
     * The starting position of the person is recorded.
     * Training phase changes from {@link TrainingPhase#READY} to {@link TrainingPhase#STARTED}.
     *
     * @param person Instance of the {@link Person} class that contains the coordinates of the
     *               {@link KeyPoint}.
     */
    void start(Person person);

    /**
     * Evaluates one repetition of the exercise.
     *
     * @param person The pose of the person in the current camera frame.
     * @return Value {@value EVALUATION_IN_PROGRESS} if the exercise is in progress or any other code representing the evaluation of the exercise.
     */
    int evaluate(Person person);

    /**
     * Finishes evaluating the exercises. Training phase changes to {@link TrainingPhase#FINISHED}.
     */
    void finish();

    /**
     * Translates the exercise evaluation code into the appropriate text message.
     *
     * @param evaluation Any code provided by the {@link #evaluate(Person)} method.
     * @return Resource ID, for example R.string.text_message
     * @throws IllegalArgumentException If the provided code is incorrect
     */
    int provideTextMessageResource(int evaluation) throws IllegalArgumentException;

    /**
     * Translates the exercise evaluation code into the appropriate voice message.
     *
     * @param evaluation Any code provided by the {@link #evaluate(Person)} method.
     * @return Resource ID, for example R.raw.voice_message
     * @throws IllegalArgumentException If the provided code is incorrect
     */
    int provideVoiceMessageResource(int evaluation) throws IllegalArgumentException;
}
