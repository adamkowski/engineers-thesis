package com.gitlab.adamkowski.squatanalyzerapp.impl;

import android.graphics.Point;
import android.util.Log;

import com.gitlab.adamkowski.squatanalyzerapp.R;
import com.gitlab.adamkowski.squatanalyzerapp.api.Training;
import com.gitlab.adamkowski.squatanalyzerapp.posenet.BodyPart;
import com.gitlab.adamkowski.squatanalyzerapp.posenet.Person;

import java.util.Arrays;
import java.util.List;

public class SquatsTraining implements Training {
    public static final int EVALUATION_DEPTH_ENOUGH = 1;
    public static final int EVALUATION_DEPTH_NOT_ENOUGH = 2;

    private static final int RIGHT_SHOULDER_ID = BodyPart.RIGHT_SHOULDER.ordinal();
    private static final int RIGHT_HIP_ID = BodyPart.RIGHT_HIP.ordinal();
    private static final int RIGHT_KNEE_ID = BodyPart.RIGHT_KNEE.ordinal();

    private static final int SHOULDER_ERROR_MARGIN = 10;
    private static final int KNEE_ERROR_MARGIN = 10;

    private TrainingPhase trainingPhase = TrainingPhase.READY;
    private Person readyPerson;
    private int lowestHipPosition = 0;
    private Point defaultShoulderPosition;

    private boolean squatInProgress = false;

    @Override
    public TrainingPhase getPhase() {
        return trainingPhase;
    }

    @Override
    public List<BodyPart> getEvaluatedBodyParts() {
        return Arrays.asList(BodyPart.RIGHT_SHOULDER, BodyPart.RIGHT_HIP, BodyPart.RIGHT_KNEE);
    }

    @Override
    public void start(Person person) {
        if (trainingPhase == TrainingPhase.READY) {
            readyPerson = person;
            defaultShoulderPosition = readyPerson.keyPoints.get(RIGHT_SHOULDER_ID).position;
            trainingPhase = TrainingPhase.STARTED;
        }
    }

    @Override
    public int evaluate(Person person) {
        final Point currentShoulderPosition = getBodyPartPosition(person, RIGHT_SHOULDER_ID);
        if (squatInProgress) {
            return evaluateSquatDepth(person, currentShoulderPosition);
        } else {
            squatInProgress = isSquatStarted(currentShoulderPosition);
        }
        return EVALUATION_IN_PROGRESS;
    }

    @Override
    public void finish() {
        trainingPhase = TrainingPhase.FINISHED;
    }

    @Override
    public int provideTextMessageResource(int evaluation) {
        if (evaluation == EVALUATION_DEPTH_ENOUGH) {
            return R.string.evaluation_squat_good;
        } else if (evaluation == EVALUATION_DEPTH_NOT_ENOUGH) {
            return R.string.evaluation_squat_insufficient_depth;
        } else {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public int provideVoiceMessageResource(int evaluation) {
        if (evaluation == EVALUATION_DEPTH_ENOUGH) {
            return R.raw.squat_good;
        } else if (evaluation == EVALUATION_DEPTH_NOT_ENOUGH) {
            return R.raw.squat_insufficient_depth;
        } else {
            throw new IllegalArgumentException();
        }
    }

    private int evaluateSquatDepth(Person person, Point currentShoulderPosition) {
        final Point hipPosition = getBodyPartPosition(person, RIGHT_HIP_ID);
        lowestHipPosition = Math.max(lowestHipPosition, hipPosition.y);

        int evaluation = EVALUATION_IN_PROGRESS;
        if (isPersonStandingAgain(currentShoulderPosition)) {
            if (isSquatDeepEnough()) {
                Log.d("training", "Deep: enough");
                evaluation = EVALUATION_DEPTH_ENOUGH;
            } else {
                Log.d("training", "Deep: not enough");
                evaluation = EVALUATION_DEPTH_NOT_ENOUGH;
            }

            squatInProgress = false;
            lowestHipPosition = 0;
        }

        return evaluation;
    }

    private Point getBodyPartPosition(Person person, int idKeyPoint) {
        return person.keyPoints.get(idKeyPoint).position;
    }

    private boolean isPersonStandingAgain(Point shoulderPosition) {
        return shoulderPosition.y - defaultShoulderPosition.y < SHOULDER_ERROR_MARGIN;
    }

    private boolean isSquatDeepEnough() {
        Point readyPersonKnee = getBodyPartPosition(readyPerson, RIGHT_KNEE_ID);
        return lowestHipPosition + KNEE_ERROR_MARGIN > readyPersonKnee.y;
    }

    private boolean isSquatStarted(Point shoulderPosition) {
        return shoulderPosition.y - defaultShoulderPosition.y >= SHOULDER_ERROR_MARGIN;
    }
}
