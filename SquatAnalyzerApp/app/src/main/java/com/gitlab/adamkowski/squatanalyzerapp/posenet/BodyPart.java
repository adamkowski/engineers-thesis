package com.gitlab.adamkowski.squatanalyzerapp.posenet;

/**
 * The key points detected by the TensorFlow
 * @see <a href="https://www.tensorflow.org/lite/models/pose_estimation/overview#how_it_works">TensorFlow pose estimation</a>
 */
public enum BodyPart {
    NOSE,
    LEFT_EYE,
    RIGHT_EYE,
    LEFT_EAR,
    RIGHT_EAR,
    LEFT_SHOULDER,
    RIGHT_SHOULDER,
    LEFT_ELBOW,
    RIGHT_ELBOW,
    LEFT_WRIST,
    RIGHT_WRIST,
    LEFT_HIP,
    RIGHT_HIP,
    LEFT_KNEE,
    RIGHT_KNEE,
    LEFT_ANKLE,
    RIGHT_ANKLE
}
