package com.gitlab.adamkowski.squatanalyzerapp.posenet;

import android.graphics.Point;

public class KeyPoint {
    public final BodyPart bodyPart;
    public final Point position;
    public final float score;

    public KeyPoint(BodyPart bodyPart, Point position, float score) {
        this.bodyPart = bodyPart;
        this.position = position;
        this.score = score;
    }
}
