package com.gitlab.adamkowski.squatanalyzerapp.posenet;

import java.util.List;

public class Person {
    public final List<KeyPoint> keyPoints;
    public final float score;

    public Person(List<KeyPoint> keyPoints, float score) {
        this.keyPoints = keyPoints;
        this.score = score;
    }
}
