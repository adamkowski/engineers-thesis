package com.gitlab.adamkowski.squatanalyzerapp.posenet;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.util.Log;
import android.util.Pair;

import org.tensorflow.lite.Interpreter;
import org.tensorflow.lite.gpu.CompatibilityList;
import org.tensorflow.lite.gpu.GpuDelegate;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class PoseEstimator {
    private static final int HEAT_MAPS_INDEX = 0;
    private static final int OFFSET_VECTORS_INDEX = 1;

    private final Interpreter interpreter;

    public PoseEstimator(Context applicationContext) {
        MappedByteBuffer poseNetModel = loadModel(applicationContext);
        this.interpreter = new Interpreter(poseNetModel, initializeInterpreterOptions());
    }

    private Interpreter.Options initializeInterpreterOptions() {
        Interpreter.Options options = new Interpreter.Options();
        CompatibilityList compatibilityList = new CompatibilityList();
        if (compatibilityList.isDelegateSupportedOnThisDevice()) {
            GpuDelegate.Options delegateOptions = compatibilityList.getBestOptionsForThisDevice();
            GpuDelegate gpuDelegate = new GpuDelegate(delegateOptions);
            options.addDelegate(gpuDelegate);
        } else {
            options.setNumThreads(4);
        }
        return options;
    }

    private MappedByteBuffer loadModel(Context applicationContext) {
        try {
            AssetFileDescriptor fileDescriptor =
                    applicationContext.getAssets().openFd(TensorFlowPoseNetModel.FILE_NAME);
            FileInputStream inputStream = new FileInputStream(fileDescriptor.getFileDescriptor());
            return inputStream.getChannel().map(FileChannel.MapMode.READ_ONLY,
                    fileDescriptor.getStartOffset(),
                    fileDescriptor.getDeclaredLength());
        } catch (IOException e) {
            Log.e("loadModel()", "Assets file error: " + e.getMessage());
        }
        return null;
    }

    public Person estimateSinglePose(Bitmap bitmap) {
        final ByteBuffer[] inputs = initInputs(bitmap);
        final HashMap<Integer, Object> outputs = initOutputs(interpreter);
        return estimateSinglePose(bitmap, inputs, outputs);
    }

    private ByteBuffer[] initInputs(Bitmap bitmap) {
        final int colors = 3;
        final int floatSize = 4;
        final int width = bitmap.getWidth();
        final int height = bitmap.getHeight();

        ByteBuffer inputBuffer = ByteBuffer.allocateDirect(height * width * colors * floatSize);
        inputBuffer.order(ByteOrder.nativeOrder());
        inputBuffer.rewind();

        int[] pixels = new int[width * height];
        bitmap.getPixels(pixels, 0, width, 0, 0, width, height);
        Arrays.stream(pixels)
                .forEach(pixel -> {
                    inputBuffer.putFloat(zScoreStandardize(pixel >> 16 & 0xFF));
                    inputBuffer.putFloat(zScoreStandardize(pixel >> 8 & 0xFF));
                    inputBuffer.putFloat(zScoreStandardize(pixel & 0xFF));
                });

        return new ByteBuffer[]{inputBuffer};
    }

    private float zScoreStandardize(int color) {
        return (color - 128.0f) / 128.0f;
    }

    private HashMap<Integer, Object> initOutputs(Interpreter interpreter) {
        HashMap<Integer, Object> outputs = new HashMap<>();

        int[] heatMapsShape = interpreter.getOutputTensor(0).shape();
        outputs.put(HEAT_MAPS_INDEX,
                new float[heatMapsShape[0]][heatMapsShape[1]][heatMapsShape[2]][heatMapsShape[3]]);

        int[] offsetsShape = interpreter.getOutputTensor(1).shape();
        outputs.put(OFFSET_VECTORS_INDEX,
                new float[offsetsShape[0]][offsetsShape[1]][offsetsShape[2]][offsetsShape[3]]);

        return outputs;
    }

    private Person estimateSinglePose(Bitmap bitmap,
                                      ByteBuffer[] inputs,
                                      HashMap<Integer, Object> outputs) {
        interpreter.runForMultipleInputsOutputs(inputs, outputs);

        float[][][][] heatMaps = (float[][][][]) outputs.getOrDefault(HEAT_MAPS_INDEX,
                new float[0][][][]);
        float[][][][] offsetVectors = (float[][][][]) outputs.get(OFFSET_VECTORS_INDEX);

        final int resolution = heatMaps[0].length; // ((InputImageSize - 1) / OutputStride) + 1
        final int keyPointsNumber = heatMaps[0][0][0].length;

        List<Pair<Integer, Integer>> keyPointsCoordinates =
                findMostLikelyKeyPointsCoordinates(heatMaps[0], keyPointsNumber, resolution);

        List<Pair<Integer, Integer>> correctedKeyPoints =
                applyVectorOffsetAdjustment(keyPointsCoordinates, resolution, bitmap.getHeight(), offsetVectors);

        double[] confidenceScores = IntStream.range(0, keyPointsNumber)
                .mapToDouble(index -> {
                    Pair<Integer, Integer> position = keyPointsCoordinates.get(index);
                    return sigmoid(heatMaps[0][position.first][position.second][index]);
                })
                .toArray();

        BodyPart[] bodyParts = BodyPart.values();
        List<KeyPoint> personKeyPoints = IntStream.range(0, keyPointsNumber)
                .mapToObj(index -> {
                    Pair<Integer, Integer> keyPoint = correctedKeyPoints.get(index);
                    return new KeyPoint(bodyParts[index],
                            new Point(keyPoint.second, keyPoint.first),
                            (float) confidenceScores[index]);
                })
                .collect(Collectors.toList());

        float totalScore = (float) Arrays.stream(confidenceScores).sum() / keyPointsNumber;

        return new Person(personKeyPoints, totalScore);
    }

    private List<Pair<Integer, Integer>> findMostLikelyKeyPointsCoordinates(float[][][] heatMaps,
                                                                            int keyPointsNumber,
                                                                            int resolution) {
        return IntStream.range(0, keyPointsNumber)
                .mapToObj(sliceNumber -> findMaxValueCoordinates(heatMaps, sliceNumber, resolution))
                .collect(Collectors.toList());
    }

    private Pair<Integer, Integer> findMaxValueCoordinates(float[][][] slice,
                                                           int sliceNumber,
                                                           int resolution) {
        float maxValue = slice[0][0][sliceNumber];
        Pair<Integer, Integer> maxValueCoordinates = new Pair<>(0, 0);
        for (int row = 0; row < resolution; row++) {
            for (int col = 0; col < resolution; col++) {
                if (slice[row][col][sliceNumber] > maxValue) {
                    maxValue = slice[row][col][sliceNumber];
                    maxValueCoordinates = new Pair<>(row, col);
                }
            }
        }
        return maxValueCoordinates;
    }

    private List<Pair<Integer, Integer>> applyVectorOffsetAdjustment(List<Pair<Integer, Integer>> keyPointsCoordinates,
                                                                     int resolution,
                                                                     int bitmapSize,
                                                                     float[][][][] offsetVectors) {
        return IntStream.range(0, keyPointsCoordinates.size())
                .mapToObj(index -> {
                    Pair<Integer, Integer> position = keyPointsCoordinates.get(index);

                    int correctedY = (int) (position.first / (float) (resolution - 1) *
                            bitmapSize + offsetVectors[0][position.first][position.second][index]);
                    int correctedX = (int) (position.second / (float) (resolution - 1) *
                            bitmapSize + offsetVectors[0][position.first][position.second][index + keyPointsCoordinates.size()]);

                    return new Pair<>(correctedY, correctedX);
                })
                .collect(Collectors.toList());
    }

    private float sigmoid(float x) {
        return 1.0f / (float) (1.0 + Math.exp(-x));
    }
}
