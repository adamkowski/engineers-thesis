package com.gitlab.adamkowski.squatanalyzerapp.posenet;

/**
 * PoseNet is a vision model that can be used to estimate the pose of a person.
 * @see <a href="https://storage.googleapis.com/download.tensorflow.org/models/tflite/posenet_mobilenet_v1_100_257x257_multi_kpt_stripped.tflite">TensorFlow Lite PoseNet</a>
 */
public class TensorFlowPoseNetModel {
    public static final String FILE_NAME = "posenet_mobilenet_v1_100_257x257_multi_kpt_stripped.tflite";
    public static final int SIZE = 257;
}
