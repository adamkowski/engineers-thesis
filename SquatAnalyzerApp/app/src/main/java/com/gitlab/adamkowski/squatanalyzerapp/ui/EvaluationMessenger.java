package com.gitlab.adamkowski.squatanalyzerapp.ui;

import android.content.Context;
import android.media.MediaPlayer;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.gitlab.adamkowski.squatanalyzerapp.R;
import com.gitlab.adamkowski.squatanalyzerapp.api.Training;

public class EvaluationMessenger {
    private final Context context;
    private final AppCompatActivity activity;
    private final Training training;

    private static MediaPlayer mediaPlayer = null;

    private static long previousTimeInMillis = System.currentTimeMillis();
    private final TextView tvDelay;

    public EvaluationMessenger(Context context, Training training) {
        this.context = context;
        this.activity = (AppCompatActivity) context;
        this.training = training;
        this.tvDelay = activity.findViewById(R.id.activity_main_tv_delay);
    }

    public void informUser(int trainingEvaluation) {
        if (trainingEvaluation != Training.EVALUATION_IN_PROGRESS) {
            int textResource = training.provideTextMessageResource(trainingEvaluation);
            showToast(textResource);
            int voiceResource = training.provideVoiceMessageResource(trainingEvaluation);
            playVoiceCommunicate(voiceResource);
        }
    }

    public void showDelayInfo() {
        long delay = System.currentTimeMillis() - previousTimeInMillis;
        activity.runOnUiThread(() -> tvDelay.setText(String.valueOf(delay)));
        previousTimeInMillis = System.currentTimeMillis();
    }

    private void showToast(int resource) {
        activity.runOnUiThread(() -> Toast.makeText(context, resource, Toast.LENGTH_SHORT).show());
    }

    private void playVoiceCommunicate(int resource) {
        mediaPlayer = MediaPlayer.create(context, resource);
        mediaPlayer.start();
    }
}
