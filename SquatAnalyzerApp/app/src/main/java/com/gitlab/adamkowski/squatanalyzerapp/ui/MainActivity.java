package com.gitlab.adamkowski.squatanalyzerapp.ui;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.camera.core.CameraSelector;
import androidx.camera.core.ImageAnalysis;
import androidx.camera.core.Preview;
import androidx.camera.core.UseCase;
import androidx.camera.lifecycle.ProcessCameraProvider;
import androidx.camera.view.PreviewView;
import androidx.core.content.ContextCompat;

import com.gitlab.adamkowski.squatanalyzerapp.R;
import com.gitlab.adamkowski.squatanalyzerapp.analyse.CameraFrameAnalyzer;
import com.google.common.util.concurrent.ListenableFuture;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MainActivity extends AppCompatActivity {
    public static final int REQUEST_CAMERA_PERMISSION = 0;

    private PreviewView cameraPreview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cameraPreview = findViewById(R.id.activity_main_pv_camera_preview);

        if (isCameraPermissionGranted()) {
            startCamera();
        } else {
            requestPermissions(new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA_PERMISSION);
        }
    }

    private boolean isCameraPermissionGranted() {
        return checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CAMERA_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startCamera();
            } else {
                String message = "Analysis cannot be performed without camera permission";
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void startCamera() {
        ListenableFuture<ProcessCameraProvider> cameraProviderFuture =
                ProcessCameraProvider.getInstance(getApplicationContext());
        cameraProviderFuture.addListener(() -> {
            try {
                ProcessCameraProvider cameraProvider = cameraProviderFuture.get();
                cameraProvider.unbindAll();
                cameraProvider.bindToLifecycle(this,
                        CameraSelector.DEFAULT_BACK_CAMERA,
                        provideCameraUseCases());
            } catch (ExecutionException | InterruptedException e) {
                Log.e("startCamera()", "Use case binding failed", e);
            }
        }, ContextCompat.getMainExecutor(this));
    }

    private UseCase[] provideCameraUseCases() {
        Preview preview = new Preview.Builder().build();
        preview.setSurfaceProvider(cameraPreview.getSurfaceProvider());

        ExecutorService cameraExecutor = Executors.newSingleThreadExecutor();
        ImageAnalysis.Analyzer cameraAnalyzer =
                new CameraFrameAnalyzer(this);

        ImageAnalysis imageAnalyzer = new ImageAnalysis.Builder().build();
        imageAnalyzer.setAnalyzer(cameraExecutor, cameraAnalyzer);

        return new UseCase[]{preview, imageAnalyzer};
    }
}
