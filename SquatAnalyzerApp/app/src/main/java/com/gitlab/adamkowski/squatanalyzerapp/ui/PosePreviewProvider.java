package com.gitlab.adamkowski.squatanalyzerapp.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PointF;
import android.util.Size;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.gitlab.adamkowski.squatanalyzerapp.R;
import com.gitlab.adamkowski.squatanalyzerapp.api.Training;
import com.gitlab.adamkowski.squatanalyzerapp.posenet.BodyPart;
import com.gitlab.adamkowski.squatanalyzerapp.posenet.KeyPoint;
import com.gitlab.adamkowski.squatanalyzerapp.posenet.Person;
import com.gitlab.adamkowski.squatanalyzerapp.posenet.TensorFlowPoseNetModel;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class PosePreviewProvider {
    private final AppCompatActivity activity;
    private final Training training;
    private final ImageView previewKeyPoints;

    private final Paint paint = new Paint();

    public PosePreviewProvider(Context context, Training training) {
        this.activity = (AppCompatActivity) context;
        this.training = training;
        this.previewKeyPoints = activity.findViewById(R.id.activity_main_iv_key_points);

        this.paint.setColor(Color.WHITE);
        this.paint.setStrokeWidth(10.0f);
    }

    public void drawPose(Person person) {
        activity.runOnUiThread(() -> previewKeyPoints.setImageBitmap(previewPoints(person)));
    }

    public void clearPreview() {
        activity.runOnUiThread(() -> previewKeyPoints.setImageBitmap(null));
    }

    private Bitmap previewPoints(Person person) {
        final Size viewSize = new Size(previewKeyPoints.getWidth(), previewKeyPoints.getHeight());
        final float widthRatio = (float) viewSize.getWidth() / TensorFlowPoseNetModel.SIZE;
        final float heightRatio = (float) viewSize.getWidth() / TensorFlowPoseNetModel.SIZE;

        Bitmap bitmap = Bitmap.createBitmap(viewSize.getWidth(),
                viewSize.getHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);

        List<BodyPart> importantBodyParts = training.getEvaluatedBodyParts();

        Map<BodyPart, KeyPoint> detectedKeyPoints = person.keyPoints.stream()
                .filter(point -> importantBodyParts.contains(point.bodyPart))
                .collect(Collectors.toMap(point -> point.bodyPart, point -> point));

        detectedKeyPoints.values()
                .forEach(keyPoint -> markPoint(canvas, keyPoint, widthRatio, heightRatio));

        connectPoints(canvas, detectedKeyPoints, importantBodyParts, widthRatio, heightRatio);

        return bitmap;
    }

    private void markPoint(Canvas canvas, KeyPoint keyPoint, float widthRatio, float heightRatio) {
        final float pointSize = 20.0f;
        PointF point = getCoordinates(keyPoint.position, widthRatio, heightRatio);
        canvas.drawCircle(point.x, point.y, pointSize, paint);
    }

    private void connectPoints(Canvas canvas,
                               Map<BodyPart, KeyPoint> points,
                               List<BodyPart> importantBodyParts,
                               float widthRatio,
                               float heightRatio) {

        for (int i = 1; i < importantBodyParts.size(); i++) {
            Point firstDot = points.get(importantBodyParts.get(i - 1)).position;
            PointF begin = getCoordinates(firstDot, widthRatio, heightRatio);

            Point secondDot = points.get(importantBodyParts.get(i)).position;
            PointF end = getCoordinates(secondDot, widthRatio, heightRatio);

            canvas.drawLine(begin.x, begin.y, end.x, end.y, paint);
        }
    }

    private PointF getCoordinates(Point point, float widthRatio, float heightRatio) {
        float x = point.x * widthRatio;
        float y = point.y * heightRatio;
        return new PointF(x, y);
    }
}
