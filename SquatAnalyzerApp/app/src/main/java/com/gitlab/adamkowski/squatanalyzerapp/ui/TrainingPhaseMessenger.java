package com.gitlab.adamkowski.squatanalyzerapp.ui;

import android.content.Context;
import android.media.MediaPlayer;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.gitlab.adamkowski.squatanalyzerapp.R;

import java.util.HashMap;
import java.util.Map;

public class TrainingPhaseMessenger {
    public enum STATE_MESSAGE {
        READY, IN_PROGRESS, FINISH
    }

    private final Context context;
    private final AppCompatActivity activity;

    private final Map<STATE_MESSAGE, TextView> stateMessages;

    private static MediaPlayer mediaPlayer = null;

    public TrainingPhaseMessenger(Context context) {
        this.context = context;
        this.activity = (AppCompatActivity) context;

        this.stateMessages = new HashMap<>(3);
        stateMessages.put(STATE_MESSAGE.READY,
                activity.findViewById(R.id.activity_main_tv_evaluating_ready));
        stateMessages.put(STATE_MESSAGE.IN_PROGRESS,
                activity.findViewById(R.id.activity_main_tv_evaluating_in_progress));
        stateMessages.put(STATE_MESSAGE.FINISH,
                activity.findViewById(R.id.activity_main_tv_evaluating_finished));
    }

    public void informUser(STATE_MESSAGE message) {
        setStateMessage(message);
        playVoiceCommunicate(message);
    }

    private void setStateMessage(STATE_MESSAGE message) {
        activity.runOnUiThread(() -> {
            stateMessages.values().forEach(textView -> textView.setVisibility(View.GONE));
            stateMessages.get(message).setVisibility(View.VISIBLE);
        });
    }

    private void playVoiceCommunicate(STATE_MESSAGE message) {
        mediaPlayer = createMediaPlayerWithRightResource(message);
        mediaPlayer.start();
    }

    private MediaPlayer createMediaPlayerWithRightResource(STATE_MESSAGE message) {
        if (message == STATE_MESSAGE.IN_PROGRESS) {
            return MediaPlayer.create(context, R.raw.training_start);
        }
        return MediaPlayer.create(context, R.raw.training_finish);
    }
}
