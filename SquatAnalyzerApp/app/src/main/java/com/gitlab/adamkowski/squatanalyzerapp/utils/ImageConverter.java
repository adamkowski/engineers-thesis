package com.gitlab.adamkowski.squatanalyzerapp.utils;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.util.Size;

import androidx.camera.core.ImageProxy;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class ImageConverter {
    private ImageConverter() {
    }

    public static Bitmap cropBitmapToSquare(Bitmap bitmap) {
        final int height = bitmap.getHeight();
        final int width = bitmap.getWidth();
        int bar = (width - height) / 2;
        return Bitmap.createBitmap(bitmap,
                bar,
                0,
                width - 2 * bar,
                height);
    }

    public static Bitmap rotateBitmapToPortraitMode(Bitmap bitmap) {
        Matrix rotateMatrix = new Matrix();
        rotateMatrix.postRotate(90.0f);
        return Bitmap.createBitmap(bitmap,
                0,
                0,
                bitmap.getWidth(),
                bitmap.getHeight(),
                rotateMatrix,
                true);
    }

    public static int[] convertFrameYUVToRGB(ImageProxy.PlaneProxy[] planes, Size imageSize) {
        final byte[][] yuvBytes = receiveBytes(planes);
        return IntStream.range(0, imageSize.getHeight())
                .flatMap(row -> getRGBRow(yuvBytes, planes, imageSize.getWidth(), row))
                .toArray();
    }

    private static IntStream getRGBRow(byte[][] yuvBytes,
                                       ImageProxy.PlaneProxy[] planes,
                                       int imageWidth,
                                       int row) {
        final int yRowStride = planes[0].getRowStride();
        final int yPixelStride = planes[0].getPixelStride();
        final int uvRowStride = planes[1].getRowStride();
        final int uvPixelStride = planes[1].getPixelStride();

        return IntStream.range(0, imageWidth)
                .map(col -> {
                    final int uvPosition = (col >> 1) * uvPixelStride + (row >> 1) * uvRowStride;
                    int y = yuvBytes[0][col * yPixelStride + row * yRowStride] & 0xff;
                    int u = yuvBytes[1][uvPosition] & 0xff;
                    int v = yuvBytes[2][uvPosition] & 0xff;
                    return convertYUVToRGBPixel(y, u, v);
                });
    }

    private static int convertYUVToRGBPixel(int y, int u, int v) {
        int y1 = (19077 * y) >> 8;
        int r = putColorInCorrectRange((y1 + ((26149 * v) >> 8) - 14234) >> 6);
        int g = putColorInCorrectRange((y1 - ((6419 * u) >> 8) - ((1665 * v) >> 5) + 8708) >> 6);
        int b = putColorInCorrectRange((y1 + ((16525 * u) >> 7) - 17685) >> 6);
        return 0xff000000 + b + (g << 8) + (r << 16);
    }

    private static int putColorInCorrectRange(int colorComponent) {
        final int MIN_VALID_VALUE = 0;
        final int MAX_VALID_VALUE = 255;
        if (colorComponent < MIN_VALID_VALUE) {
            return MIN_VALID_VALUE;
        }
        return Math.min(colorComponent, MAX_VALID_VALUE);
    }

    private static byte[][] receiveBytes(ImageProxy.PlaneProxy[] planes) {
        return Arrays.stream(planes)
                .map(ImageProxy.PlaneProxy::getBuffer)
                .map(ImageConverter::transferBytesToArray)
                .toArray(byte[][]::new);
    }

    private static byte[] transferBytesToArray(ByteBuffer buffer) {
        return Stream.of(new byte[buffer.capacity()])
                .peek(buffer::get)
                .findFirst()
                .get();
    }
}
